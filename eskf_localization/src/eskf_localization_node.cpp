//
// Created by ZhaoXiaoFei on 2022/8/18.
//
#include <iostream>
#include <ros/ros.h>
#include <glog/logging.h>
#include "front_end/front_end.hpp"
#include <memory>

int main(int argc, char* argv[]){
    FLAGS_log_dir = "/home/ubuntu/WorkSpace/LSlam/eskf_ws/src/eskf_localization/Log";
    google::InitGoogleLogging("eskf_localization_log");
    FLAGS_alsologtostderr = true;

    ros::init(argc, argv, "eskf_localization");
    ros::NodeHandle nh("~");
    LocalizationFrontEnd localizationFrontEnd(nh);

    ros::spin();
    return 0;
}


