//
// Created by ZhaoXiaoFei on 2022/8/18.
//

#ifndef ESKF_LOCALIZATION_PREDEFINEDATA_HPP
#define ESKF_LOCALIZATION_PREDEFINEDATA_HPP
#include <Eigen/Dense>
struct ImuData{
    double timestamp;

    Eigen::Vector3d acc;
    Eigen::Vector3d gyr;
};

struct GpsPositionData{
    double timestamp;

    Eigen::Vector3d lla;		//Unit: Latitude,Longitude(degrees); Altitude(meters)
    Eigen::Matrix3d cov;
};


struct MagData{
    double timestamp;

    Eigen::Vector3d mag;		//Unit: uT
};

struct State{
    double timestamp;

    //Nominal states
    Eigen::Matrix4d pose_;
    Eigen::Vector3d vel_;
    Eigen::Vector3d acc_bias_;
    Eigen::Vector3d gyr_bias_;

    Eigen::Vector3d initLLA;

    Eigen::Matrix<double,15,1> X_;
    Eigen::Matrix<double,15,15> P_;

    ImuData imuData;
};
#endif //ESKF_LOCALIZATION_PREDEFINEDATA_HPP
