//
// Created by ZhaoXiaoFei on 2022/8/19.
//

#ifndef ESKF_LOCALIZATION_GPSCORRECT_HPP
#define ESKF_LOCALIZATION_GPSCORRECT_HPP
#include <Eigen/Dense>
#include "preDefineData.hpp"
#include <glog/logging.h>
#include <GeographicLib/LocalCartesian.hpp>

class GpsCorrect{
public:
    GpsCorrect();
    void correct(const GpsPositionData& GpsData, State* state);

};

#endif //ESKF_LOCALIZATION_GPSCORRECT_HPP
