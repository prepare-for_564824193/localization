//
// Created by ZhaoXiaoFei on 2022/8/19.
//

#ifndef ESKF_LOCALIZATION_IMUPREDICT_HPP
#define ESKF_LOCALIZATION_IMUPREDICT_HPP
#include <Eigen/Dense>
#include <sophus/so3.hpp>
#include "preDefineData.hpp"
#include <glog/logging.h>

class ImuPredict{
public:
    ImuPredict(const double acc_noise, const double gyr_noise,
                 const double acc_bias_noise, const double gyr_bias_noise, const Eigen::Vector3d G);

    void predict(const ImuData imu_data, State* state);

private:
    double acc_noise_;	//accelerometer measurement noise
    double gyr_noise_;	//gyroscope measurement noise
    double acc_bias_noise_;	//accelerometer bias noise
    double gyr_bias_noise_;	//gyroscope bias noise
    Eigen::Vector3d G_;
};


#endif //ESKF_LOCALIZATION_IMUPREDICT_HPP
