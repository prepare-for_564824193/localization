//
// Created by ZhaoXiaoFei on 2022/8/18.
//

#ifndef ESKF_LOCALIZATION_INIT_HPP
#define ESKF_LOCALIZATION_INIT_HPP
#include "preDefineData.hpp"
#include <queue>
#include <glog/logging.h>

class Initializer{
public:
    Initializer(int num, State* state_);
    void Imu_initialize(const ImuData& imu_data);
    void Gps_initialize(const GpsPositionData& gps_data);
    void Mag_initialize(const MagData& mag_data);

    bool isInit();

private:
    int needImuNum;
    int needGpsNum;
    int needMagNum;
    bool isRecvFirstGps = false;

    bool isImuInit = false;
    bool isGpsInit = false;
    bool isMagInit = false;
    bool isESKFInit = false;

    std::deque<ImuData> imu_buffer_;
    std::deque<GpsPositionData> gps_buffer_;
    std::deque<MagData> mag_buffer_;

    State* state;
};

#endif //ESKF_LOCALIZATION_INIT_HPP
